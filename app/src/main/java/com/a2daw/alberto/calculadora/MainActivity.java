package com.a2daw.alberto.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    // Variables
    private final int TOTAL_NUMEROS_PANTALLA = 19;
    private final String SUMA = "SUMA";
    private final String RESTA = "RESTA";
    private final String MULTI = "MULTIPLICACIÓN";
    private final String DIVISION = "DIVISIÓN";

    // Datos
    private BigDecimal operando1;
    private BigDecimal operando2;
    private String operador;
    private boolean resetNumero = false;
    private DecimalFormat formato = new DecimalFormat("#.#################");
    private DecimalFormatSymbols simbolo = DecimalFormatSymbols.getInstance();
    private MathContext ajusteNumero = new MathContext(19, RoundingMode.HALF_UP);

    // Elementos
    private List<Button> botonesNumericos;
    private static final int[] BOTONES_NUMERICOS_IDS = {
            R.id.boton0,
            R.id.boton1,
            R.id.boton2,
            R.id.boton3,
            R.id.boton4,
            R.id.boton5,
            R.id.boton6,
            R.id.boton7,
            R.id.boton8,
            R.id.boton9
    };
    private List<Button> botonesOperadores;
    private static final int[] BOTONES_OPERADORES_IDS = {
            R.id.botonSuma,
            R.id.botonResta,
            R.id.botonMulti,
            R.id.botonDivision
    };
    private Button  btnIgual, btnComa, btnC, btnMasMenos;
    private TextView pantallaNum, pantallaOp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Configuración de formato
        // Hacemos que el separador sea una coma
        simbolo.setDecimalSeparator(',');
        formato.setDecimalFormatSymbols(simbolo);
        //En caso de que el numero sea demasiado grande, se redondeará hacia arriba
        formato.setRoundingMode(RoundingMode.HALF_UP);

        // Asignacion de elementos
        botonesNumericos = new ArrayList<Button>();
        for(int id : BOTONES_NUMERICOS_IDS) {
            Button boton = (Button)findViewById(id);
            boton.setOnClickListener(agregaNumero);
            botonesNumericos.add(boton);
        }

        botonesOperadores = new ArrayList<Button>();
        for(int id : BOTONES_OPERADORES_IDS) {
            Button boton = (Button)findViewById(id);
            boton.setOnClickListener(asignaOperacion);
            botonesOperadores.add(boton);
        }

        pantallaNum = (TextView) findViewById(R.id.TxtPantallaNum);
        pantallaOp = (TextView) findViewById(R.id.TxtPantallaOp);

        btnIgual = (Button) findViewById(R.id.botonIgual);
        btnIgual.setOnClickListener(resuelveOperacion);

        btnComa = (Button) findViewById(R.id.botonComa);
        btnComa.setOnClickListener(agregaComa);

        btnC = (Button) findViewById(R.id.botonC);
        btnC.setOnClickListener(resetCalculadora);

        btnMasMenos = (Button) findViewById(R.id.botonMasMenos);
        btnMasMenos.setOnClickListener(inversoOperando);
    }

    // Funciones

    /**
     * Asigna el dato al operando correspondiente
     * @param dato Dato a asignar
     */
    private void asignaDatos(BigDecimal dato) {
        if(operador != null) {
            operando2 = dato;
        } else {
            operando1 = dato;
        }
    }

// Listenners
    /**
     * Escribe un numero por pantalla y lo asigna a la variable correspondiente
     */
    private View.OnClickListener agregaNumero = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button boton = (Button) v;
            String txtPantalla = pantallaNum.getText().toString();
            String txtBoton = boton.getText().toString();
            BigDecimal numero;
            if(txtPantalla.length() >= TOTAL_NUMEROS_PANTALLA) {
                return;
            }
            if(txtPantalla.equals("0") || txtPantalla.isEmpty() || txtPantalla.equals("Error") || txtPantalla.equals("Infinito") || resetNumero) {
                numero = new BigDecimal(txtBoton);
                pantallaNum.setText(txtBoton);
            } else {
                String operando = txtPantalla.concat(txtBoton);
                pantallaNum.setText(operando);
                operando = formato.parse(operando, new ParsePosition(0)).toString();
                numero = new BigDecimal(operando);
            }
            asignaDatos(numero);
            resetNumero = false;
        }
    };

    /**
     * Asigna el operando en la variable y lo muestra en pantalla
     */
    private View.OnClickListener asignaOperacion = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.botonSuma:
                    pantallaOp.setText(SUMA);
                    operador = SUMA;
                    break;
                case R.id.botonResta:
                    pantallaOp.setText(RESTA);
                    operador = RESTA;
                    break;
                case R.id.botonMulti:
                    pantallaOp.setText(MULTI);
                    operador = MULTI;
                    break;
                case R.id.botonDivision:
                    pantallaOp.setText(DIVISION);
                    operador = DIVISION;
                    break;
            }
            if(operando2 == null) {
                pantallaNum.setText("");
            }
            resetNumero = false;
        }
    };

    /**
     * Resuelve la operación almacenada y muestra el resultado por pantalla
     */
    private View.OnClickListener resuelveOperacion = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(operando1 == null || operando2 == null || operador == null) return;
            if(operando1.compareTo(BigDecimal.ZERO) == 0 && operando2.compareTo(BigDecimal.ZERO) == 0 && operador.equals(DIVISION)){
                pantallaNum.setText("Error");
                operando1 = null;
                operando2 = null;
                operador = null;
                resetNumero = true;
                pantallaOp.setText("Operación");
                return;
            }
            BigDecimal resultado = null;
            switch (operador){
                case SUMA:
                    resultado = operando1.add(operando2);
                    break;
                case RESTA:
                    resultado = operando1.subtract(operando2);
                    break;
                case MULTI:
                    resultado = operando1.multiply(operando2);
                    break;
                case DIVISION:
                    if(operando2.compareTo(BigDecimal.ZERO) == 0) {
                        pantallaNum.setText("Infinito");
                        operando1 = null;
                        operando2 = null;
                        operador = null;
                        resetNumero = true;
                        pantallaOp.setText("Operación");
                        return;
                    } else {
                        resultado = operando1.divide(operando2, ajusteNumero);
                    }
                    break;
            }
            operando1 = resultado;
            operando2 = null;
            operador = null;
            resetNumero = true;
            pantallaNum.setText(formato.format(resultado));
            pantallaOp.setText("Operación");
        }
    };

    /**
     * Limpia la pantalla y reasigna las variables a sus valores predefinidos
     */
    private View.OnClickListener resetCalculadora = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            pantallaOp.setText("Operación");
            pantallaNum.setText("0");
            operando1 = null;
            operando2 = null;
            operador = null;
        }
    };

    /**
     * Escribe el inverso del numero por la pantalla y se lo asigna a la variable correspondiente
     */
    private View.OnClickListener inversoOperando = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String txtPantalla = pantallaNum.getText().toString();
            BigDecimal numero;
            if(txtPantalla.equals("0")) {
                return;
            }
            txtPantalla = pantallaNum.getText().toString();
            txtPantalla = formato.parse(txtPantalla, new ParsePosition(0)).toString();
            numero = new BigDecimal(txtPantalla).negate();
            pantallaNum.setText(formato.format(numero));
            asignaDatos(numero);
        }
    };

    /**
     * Escribe una coma en la pantalla
     */
    private View.OnClickListener agregaComa = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String txtPantalla = pantallaNum.getText().toString();
            if(txtPantalla.length() >= TOTAL_NUMEROS_PANTALLA) {
                return;
            }
            if(!txtPantalla.contains(",")) {
                pantallaNum.setText(txtPantalla.concat(","));
                resetNumero = false;
            }
        }
    };
}


